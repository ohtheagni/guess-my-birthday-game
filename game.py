
#Ask name with prompt Hi! What is your name?

user_name = input('Hi! What is your name? ')

#Allow the computer to guess 5 times

for guess_bday in range(1, 6):

    #Generate random number between 1924~2004 for years and 1~12 for months
    #by importing randomint within the for loop ensures that a new number is generated every iteration
    from random import randint
    pc_guess_year = randint(1924, 2004)
    pc_guess_month = randint(1,12)

    #We want the string to not display Guess 0 but since guess_bday is zero indexed we add 1 to ensure it doesnt print guess 0
    guess_point = guess_bday + 1

    #Prompt user with the question and store the input
    question = 'Guess ' + str(guess_bday) + ':  ' + user_name + " were you born in " + str(pc_guess_month) + ' / ' + str(pc_guess_year) + '? '
    print(question)
    answer = input('yes or no? ')

    #create conditions for yes and no to print out the following statements
    if answer == 'yes':
        print('I knew it!')
        exit()
    elif guess_bday == 5:
        print('I have other things to do. Good bye.')
    else:
        print('Drat! Lemme try again!')
